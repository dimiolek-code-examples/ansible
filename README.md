Ansible playbooks example 
======================================

Example playbook to show some basic skills I have in Ansible/DevOps.

Main playbook install some basic tools like 7zip and notepad++, also some libraries are installed like python or java, then filebeat from ELK is installed and configured for appX with example of jinja template.

## Configuration

To work with this repo you have to create hosts.yml in root of project e.g.:

```
server:
  hosts:
    'XXX.XXX.XXX.XXX':
      ansible_user: user
      ansible_password: password
      ansible_connection: winrm
      ansible_shell_type: powershell
```

## Example usage

To run main script use ansible-playbook command:

`ansible-playbook -i hosts.yml playbooks/clients/dimiolek/server/windows.yml`
